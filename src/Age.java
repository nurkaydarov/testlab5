public class Age {

    private int age;

    public int calculateAge(int currentYear, int age)
    {

        this.age = age;

        if (age > 0)
        {
            return currentYear - age;

        }
        return 0;
    }


}
